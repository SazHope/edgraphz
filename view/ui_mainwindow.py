##Copyright 2019, 2019 DUBETTIER Adrien adriendub@yahoo.fr
##Copyright 2019, 2019 LECLERC Benjamin benjamin.leclerc7@gmail.com
##
##This file is part of EdGraphZ.
##
##    EdGraphZ is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    EdGraphZ is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with EdGraphZ.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-

# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


from view.mainview import MainView


class Ui_MainWindow(object):
    """
    create the MainWindow's template

    """

    def setupUi(self, main_window):
        """
        The whole window is create in here. Widgets, views, placing.

        @param main_window: the window that will use Ui_MainWindow

        """
        main_window.setObjectName("main_window")
        main_window.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(main_window)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        

        self.mainView = MainView(self.centralwidget)
        self.mainView.setObjectName("mainView")
        self.horizontalLayout.addWidget(self.mainView)
        main_window.setCentralWidget(self.centralwidget)
        
        self.dockWidget = QtWidgets.QDockWidget(main_window)
        self.dockWidget.setFeatures(QtWidgets.QDockWidget.DockWidgetFloatable | QtWidgets.QDockWidget.DockWidgetMovable)
        self.dockWidget.setAllowedAreas(QtCore.Qt.LeftDockWidgetArea | QtCore.Qt.RightDockWidgetArea)
        self.dockWidget.setObjectName("dockWidget")

        self.dockWidgetContents = QtWidgets.QWidget()
        self.dockWidgetContents.setObjectName("dockWidgetContents")

        self.verticalLayout = QtWidgets.QVBoxLayout(self.dockWidgetContents)
        self.verticalLayout.setObjectName("verticalLayout")

        self.checkBoxButtonIsOriented = QtWidgets.QCheckBox(self.dockWidgetContents)
        self.checkBoxButtonIsOriented.setChecked(False)
        self.checkBoxButtonIsOriented.setObjectName("checkBoxButtonIsOriented")
        self.verticalLayout.addWidget(self.checkBoxButtonIsOriented)

        self.pushButtonSelection = QtWidgets.QPushButton(self.dockWidgetContents)
        self.pushButtonSelection.setObjectName("pushButtonSelection")
        self.verticalLayout.addWidget(self.pushButtonSelection)

        self.pushButtonDelete = QtWidgets.QPushButton(self.dockWidgetContents)
        self.pushButtonDelete.setObjectName("pushButtonDelete")
        self.verticalLayout.addWidget(self.pushButtonDelete)

        self.pushButtonCreateNode = QtWidgets.QPushButton(self.dockWidgetContents)
        self.pushButtonCreateNode.setObjectName("pushButtonCreateNode")
        self.verticalLayout.addWidget(self.pushButtonCreateNode)

        self.pushButtonCreateEdge = QtWidgets.QPushButton(self.dockWidgetContents)
        self.pushButtonCreateEdge.setObjectName("pushButtonCreateEdge")
        self.verticalLayout.addWidget(self.pushButtonCreateEdge)

        self.pushButtonChangeColor = QtWidgets.QPushButton(self.dockWidgetContents)
        self.pushButtonChangeColor.setObjectName("pushButtonChangeColor")
        self.verticalLayout.addWidget(self.pushButtonChangeColor)

        self.pushButtonChangeShape = QtWidgets.QPushButton(self.dockWidgetContents)
        self.pushButtonChangeShape.setObjectName("pushButtonChangeShape")
        self.verticalLayout.addWidget(self.pushButtonChangeShape)

        self.pushButtonGenerateDOT = QtWidgets.QPushButton(self.dockWidgetContents)
        self.pushButtonGenerateDOT.setObjectName("pushButtonGenerateDOT")
        self.verticalLayout.addWidget(self.pushButtonGenerateDOT)

        self.pushButtonImportDOT = QtWidgets.QPushButton(self.dockWidgetContents)
        self.pushButtonImportDOT.setObjectName("pushButtonImportDOT")
        self.verticalLayout.addWidget(self.pushButtonImportDOT)


        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)

        self.globalView = QtWidgets.QGraphicsView(self.dockWidgetContents)
        self.globalView.setObjectName("globalView")
        self.verticalLayout.addWidget(self.globalView)

        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)

        self.dockWidget.setWidget(self.dockWidgetContents)
        main_window.addDockWidget(QtCore.Qt.DockWidgetArea(1), self.dockWidget)

        self.retranslateUi(main_window)
        QtCore.QMetaObject.connectSlotsByName(main_window)

    def retranslateUi(self, main_window):
        """
        This method set all the window's names.

        @param main_window: the window that will use Ui_MainWindow
        
        
        """
        _translate = QtCore.QCoreApplication.translate
        main_window.setWindowTitle(_translate("main_window", "EdGraphZ"))
        self.checkBoxButtonIsOriented.setText(_translate("main_window", "Directed Graph"))
        self.pushButtonSelection.setText(_translate("main_window", "Selection "))
        self.pushButtonDelete.setText(_translate("main_window", "Delete Element"))
        self.pushButtonCreateNode.setText(_translate("main_window", "Create node"))
        self.pushButtonCreateEdge.setText(_translate("main_window", "Create edge"))
        self.pushButtonChangeColor.setText(_translate("main_window", "Change Color"))
        self.pushButtonChangeShape.setText(_translate("main_window", "Change Shape"))
        self.pushButtonGenerateDOT.setText(_translate("main_window", "Generate DOT"))
        self.pushButtonImportDOT.setText(_translate("main_window", "Import DOT"))

