##Copyright 2019, 2019 DUBETTIER Adrien adriendub@yahoo.fr
##Copyright 2019, 2019 LECLERC Benjamin benjamin.leclerc7@gmail.com
##
##This file is part of EdGraphZ.
##
##    EdGraphZ is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    EdGraphZ is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with EdGraphZ.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
# MainWindow.py

from PyQt5.QtCore import pyqtSlot, QMimeData

from view.ui_mainwindow import Ui_MainWindow

from model.convertDOT import ConvertDOT

##################modif#######################
from view.graphicsnode import *
from view.graphicsedge import *

##################modif#######################
from model.node import Node


class MainWindow(QMainWindow, Ui_MainWindow):

    def __init__(self, editor):
        """
        Constructor

        :param current_state: model
        """
        super().__init__()
        self.editor = editor
        self.setupUi(self)
        self.map_to_scene = {}
        self.scene = GraphicsGraph(editor, self.map_to_scene)
        for vue in (self.mainView, self.globalView):
            vue.setScene(self.scene)
            vue.setRenderHints(QPainter.Antialiasing)
        self.globalView.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.globalView.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scene.selectionChanged.connect(self.onSceneSelectionChanged)
        self.onSceneSelectionChanged()
        self.scene.refresh_main_window()
        self.show()

    @pyqtSlot()
    def on_pushButtonSelection_clicked(self):
        """
        Make action when the button selection was clicked

        All item of the scene can be movable
        The type of action is set to selection
        """
        self.editor.type_action = "selection"
        for item in self.scene.items():
            item.setFlag(QGraphicsItem.ItemIsMovable, True)

    @pyqtSlot()
    def on_pushButtonCreateNode_clicked(self):
        """
        Make action when the button createNode was clicked

        All items can't be moved
        The type of action is set to node
        """
        self.editor.type_action = "node"
        for item in self.scene.items():
            item.setFlag(QGraphicsItem.ItemIsMovable, False)

    @pyqtSlot()
    def on_pushButtonCreateEdge_clicked(self):
        """
        Make action when the button createEdge was clicked

        All items can't be moved
        The type of action is set to edge
        """
        self.editor.type_action = "edge"
        for item in self.scene.items():
            item.setFlag(QGraphicsItem.ItemIsMovable, False)

    @pyqtSlot()
    def on_pushButtonChangeColor_clicked(self):
        """
         Make action when the button ChangeColor was clicked

        All items can't be moved
        The type of action is set to color

        Replace the actual color of the node by a new one and refresh the view
        """
        self.editor.type_action = "color"
        for item in self.scene.items():
            item.setFlag(QGraphicsItem.ItemIsMovable, False)
        selectedItems = self.scene.selectedItems()
        if isinstance(selectedItems[0], (GraphicsEdge, GraphicsSelfEdge)):
            initColor = Qt.black
            color = QColorDialog.getColor(initColor, self, 'change the color')
        else:
            initColor = selectedItems[0].brush().color()
            color = QColorDialog.getColor(initColor, self, 'change the colors')
        if color.isValid():
            for item in selectedItems:
                if isinstance(item,(GraphicsNode,SquareNode, DiamondNode, TriangleNode)):
                    pen = QBrush(color)
                    item.setBrush(pen)
                    nodes = self.map_to_scene
                    nodes[item].color = pen
                elif isinstance(item, (GraphicsEdge, GraphicsSelfEdge)):
                    pen = color
                    item.setPen(pen)
                    for key, value in self.map_to_scene.items():
                        if item == key:
                            for edge in self.editor.all_edges:
                                if value == edge.id:
                                    edge.color = pen
        self.scene.refresh_main_window()
    @pyqtSlot()
    def on_pushButtonChangeShape_clicked(self):
        """
        Make action when the button ChangeColor was clicked

        All items can't be moved
        The type of action is set to color

        Replace the actual color of the node in the model by a new one and refresh the view
        """
        self.editor.type_action = "form"
        selectedItems = self.scene.selectedItems()
        shapes = ("ellipse", "square", "diamond", "triangle")
        newShape, okPressed = QInputDialog.getItem(self, "Get item", "form:", shapes, 0, False)
        if okPressed and newShape:
            for item in selectedItems:
                self.map_to_scene[item].form = newShape
        for item in self.scene.items():
            item.setFlag(QGraphicsItem.ItemIsMovable, False)
        self.scene.refresh_main_window()

    @pyqtSlot()
    def on_pushButtonGenerateDOT_clicked(self):
        """
        Make Action when the button Generate DOT is clicked

        Create an instance of ConvertDOT with the model and launch the method write_dot()
        """
        try:
            convert = ConvertDOT(self, self.editor)
            filename_to_save = QFileDialog.getSaveFileName(self, "DOT file", "", "*.dot")
            convert.write_dot(filename_to_save[0])
        except:
            print("error generation dot")

    @pyqtSlot()
    def on_pushButtonImportDOT_clicked(self):
        """
        Make Action when the button Import DOT is clicked

        Create an instance of ImportDOT the file dot in the model

        The main_window is refreshed
        """

        from model.importDOT import ImportDOT
        try:
            filename = QFileDialog.getOpenFileName(self, "DOT file", "", "*.dot")
            importDOT = ImportDOT(filename[0], self.editor, self.map_to_scene)
            importDOT.get_DOT(0)
        except:
            print("error importation dot")

        self.scene.refresh_main_window()

    @pyqtSlot()
    def on_pushButtonDelete_clicked(self):
        """
        Make Action when the button ButtonDelete is clicked

        Deletes the nodes that were selected in the interface in the model

        The main_window is refreshed (update)
        """
        self.editor.type_action = "delete"
        selectedItems = self.scene.selectedItems()
        for item in selectedItems:
            if isinstance(item, (GraphicsNode, SquareNode, DiamondNode, TriangleNode)):
                self.editor.remove_node_element(self.map_to_scene[item])
            else : 
                self.editor.remove_edge_element(self.map_to_scene[item])
        self.scene.refresh_main_window()

    @pyqtSlot()
    def onSceneSelectionChanged(self):
        """
        Update the status bar for known how many elements are selected
        """
        nbSelectedItems = len(self.scene.selectedItems())
        self.pushButtonChangeColor.setEnabled(nbSelectedItems > 0)
        self.pushButtonChangeShape.setEnabled(nbSelectedItems > 0)
        msg = '%d elements selected' % nbSelectedItems
        self.statusBar().showMessage(msg)

    @pyqtSlot(bool)
    def on_checkBoxButtonIsOriented_clicked(self, b):
        """
        Make Action when the checkBox is clicked.

        If it was clicked the model is set to oriented.

        The main_window is refreshed (update)
        :param b: boolean
        """
        self.editor.is_oriented = b
        self.scene.refresh_main_window()



class GraphicsGraph(QGraphicsScene):
    """
    A custom QGraphicsScene.

    """

    def __init__(self, editor, map_to_scene):
        """
        Constructor

        @param current_state: a reference to the model
        @param pair_model_node: a dictionary linking the view and the model
        """
        QGraphicsScene.__init__(self)
        self.editor = editor
        self.map_to_scene = map_to_scene
        self.currentPos = 0.

    def dragMoveEvent(self, e):
        """
        Allow the user to move the view with a drag and drop

        @param e: the mouse event

        """
        if e.mimeData().hasText():
            if e.mimeData().text() == "scene":
                move = self.currentPos - e.screenPos()
                self.currentPos = e.screenPos()
                hScrollBarView = self.views()[0].hScrollBar
                vScrollBarView = self.views()[0].vScrollBar
                hScrollBarView.setValue(hScrollBarView.value() + move.x())
                vScrollBarView.setValue(vScrollBarView.value() + move.y())
            else:
                e.ignore()
        else:
            self.parentItem().mainView.dragMoveEvent(e)

    def mouseMoveEvent(self, mouse_event):
        super().mouseMoveEvent(mouse_event)

    def mousePressEvent(self, mouse_event):
        """
        Allow the user to add some items to the scene

        @param mouse_event: the mouse event

        """
        super().mousePressEvent(mouse_event)
        item = self.itemAt(mouse_event.scenePos(), QTransform())
        modifiers = QApplication.keyboardModifiers()
        if item == None and modifiers == Qt.ShiftModifier:
            mimeData = QMimeData()
            drag = QDrag(mouse_event.widget())
            mimeData.setText("scene")
            drag.setMimeData(mimeData)
            drag.exec_(Qt.MoveAction)

        if mouse_event.button() == Qt.LeftButton:
            if self.editor.type_action == "node":
                self.editor.add_node_element(None, mouse_event.scenePos().x(), mouse_event.scenePos().y(), 
                    "Noeud", QColor("#FFFFFF"), "ellipse", self.editor.increment_num_id(), None)
                self.refresh_main_window()
            elif self.editor.type_action == "edge":
                for item in self.items():
                    item.setFlag(QGraphicsItem.ItemIsMovable, False)
                if modifiers == Qt.ShiftModifier:
                    for item in self.selectedItems():
                        item.setFlag(QGraphicsItem.ItemIsMovable, True)
        elif mouse_event.button() == Qt.RightButton:
            pass
        else :
            mouse_event.ignore()

    def dragEnterEvent(self, e):
        """
        Allow the user to move the view with a drag and drop

        @param e: the mouse event

        """
        if e.mimeData().hasText() and e.mimeData().text() == "scene":
            self.currentPos = e.screenPos()
        else:
            e.ignore()

    def mouseDoubleClickEvent(self, mouse_event):
        """
        Called when double-clicked on the scene

        @param mouse-event: the mouse event

        """
        super().mouseDoubleClickEvent(mouse_event)
        item = self.focusItem()
        if item is not None:
            item.mouseDoubleClickEvent(mouse_event)
        elif mouse_event.button() == Qt.LeftButton and self.itemAt(mouse_event.scenePos(), QTransform()) is None:
            self.editor.add_node_element(None, mouse_event.scenePos().x(), mouse_event.scenePos().y(), 
                    "Noeud", QColor("#FFFFFF"), "ellipse", self.editor.increment_num_id(), None)
            self.refresh_main_window()
        else:
            mouse_event.ignore()

    def keyPressEvent(self, e):
        super().keyPressEvent(e)
        self.editor.type_action = "delete"
        selected_items = self.selectedItems()
        if len(selected_items) != 0:
            if e.key() == Qt.Key_Delete:
                for item in selected_items:
                    if isinstance(item,(GraphicsNode, SquareNode, DiamondNode, TriangleNode)):
                        self.editor.remove_node_element(self.map_to_scene[item])
                    else : 
                        self.editor.remove_edge_element(self.map_to_scene[item])
                self.refresh_main_window()
        else:
            if self.focusItem() != None:
                self.focusItem().keyPressEvent(e)

    def refresh_main_window(self):
        """
        When called, repaint the scene

        """

        selected_items = self.recall_selected()
        self.clear()
        self.map_to_scene.clear() 
        for node in self.editor.all_nodes:
            node_graphic = graphicsnode(node.form, self.editor, self.map_to_scene, node.label)
            node_graphic.setPos(node.position_x, node.position_y)
            node_graphic.setFlag(QGraphicsItem.ItemIsMovable)
            node_graphic.setFlag(QGraphicsItem.ItemIsSelectable)
            node_graphic.setBrush(node.color)
            self.addItem(node_graphic) 
            self.map_to_scene[node_graphic] = node
            self.editor.all_nodes[node] = node_graphic
        self.refresh_edge_window()
        for item in selected_items :
            if isinstance(item, Edge):
                self.editor.all_edges[item].setSelected(selected_items[item])
            else :
                self.editor.all_nodes[item].setSelected(selected_items[item])

    def refresh_edge_window(self):
        """
        When called, repaint all edges

        """
        for element in self.items():
            if isinstance(element, (GraphicsEdge, GraphicsSelfEdge)):
                self.removeItem(element)

        node1_to_construct = None
        node2_to_construct = None
        for edge in self.editor.all_edges :
            graphic_node_a = self.editor.all_nodes[edge.node_a]
            graphic_node_b = self.editor.all_nodes[edge.node_b]
            if edge.node_a == edge.node_b :
                graphic_edge = GraphicsSelfEdge(graphic_node_a, graphic_node_b, self.editor,
                                            self.map_to_scene,
                                            edge.label, edge.color, edge.form)
            else :
                graphic_edge = GraphicsEdge(graphic_node_a, graphic_node_b, self.editor,
                                        self.map_to_scene,
                                        edge.label, edge.color, edge.form)
            self.map_to_scene[graphic_edge] = edge
            self.editor.all_edges[edge] = graphic_edge
            self.addItem(graphic_edge)

    def recall_selected(self):
        selected = {}
        for nodes in self.editor.all_nodes :
            if (self.editor.all_nodes[nodes] is not None) and (self.editor.all_nodes[nodes].isSelected()):
                selected[nodes] = True
            else :
                selected[nodes] = False
        for edges in self.editor.all_edges :
            if (self.editor.all_edges[edges] is not None) and (self.editor.all_edges[edges].isSelected()):
                selected[edges] = True
            else :
                selected[edges] = False
        return selected