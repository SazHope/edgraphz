##Copyright 2019, 2019 DUBETTIER Adrien adriendub@yahoo.fr
##Copyright 2019, 2019 LECLERC Benjamin benjamin.leclerc7@gmail.com
##
##This file is part of EdGraphZ.
##
##    EdGraphZ is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    EdGraphZ is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with EdGraphZ.  If not, see <https://www.gnu.org/licenses/>.

from PyQt5.QtGui import *;
from PyQt5.QtWidgets import *;


class ClickListener:
    """
    A class used to group all the listeners links to actions one can perform

    """
    
    def on_pushButtonCreerDisque_clicked(self):
        """
        Triggers when clicking on the button Create Node

        """
        # disque = self.scene.addEllipse(0,0,20,20,brush=QBrush(Qt.white),pen=QPen(Qt.NoPen))
        disque = self.scene.addEllipse(0, 0, 20, 20, brush=QBrush(Qt.white), pen=QPen())
        label = QGraphicsSimpleTextItem("label", parent=disque)
        font = QFont("Times", pointSize=5)
        label.setFont(font)
        label.setPos(3, 18)
        disque.setFlag(QGraphicsItem.ItemIsMovable)
        disque.setFlag(QGraphicsItem.ItemIsSelectable)

    def on_pushButtonCreerArete_clicked(self):
        """
        Triggers when clicking on the button Create Edge
        
        """
        itemsSelectionnes = self.scene.selectedItems()
        for item in itemsSelectionnes:
            item.setFlag(QGraphicsItem.ItemIsMovable, False)

    def on_pushButtonChangerCouleur_clicked(self):
        """
        Triggers when clicking on the button Change Color
        
        """
        itemsSelectionnes = self.scene.selectedItems()
        couleurInit = itemsSelectionnes[0].brush().color()
        couleur = QColorDialog.getColor(couleurInit, self, 'changer la couleur')
        if couleur.isValid():
            pinceau = QBrush(couleur)
            for item in itemsSelectionnes:
                item.setBrush(pinceau)

    def onSceneSelectionChanged(self):
        """
        Triggers when the selection made by the user change
        
        """
        nbElementSelectionnes = len(self.scene.selectedItems())
        self.pushButtonChangerCouleur.setEnabled(nbElementSelectionnes > 0)
        msg = '%d éléments sélectionnés' % nbElementSelectionnes
        self.statusBar().showMessage(msg)

    def on_horizontalSliderZoom_valueChanged(self, nouvZoomPctVue):
        """
        Triggers when using the slider
        
        """
        f = (nouvZoomPctVue / 100.) / self.zoomPctVue
        self.vuePrincipale.scale(f, f)
        self.zoomPctVue = nouvZoomPctVue / 100.
