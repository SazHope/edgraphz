##Copyright 2019, 2019 DUBETTIER Adrien adriendub@yahoo.fr
##Copyright 2019, 2019 LECLERC Benjamin benjamin.leclerc7@gmail.com
##
##This file is part of EdGraphZ.
##
##    EdGraphZ is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    EdGraphZ is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with EdGraphZ.  If not, see <https://www.gnu.org/licenses/>.

from PyQt5 import QtWidgets
##from pydot import Dot, Graph, graph_from_dot_file

from controller.clicklistener import ClickListener
from view.ui_mainwindow import Ui_MainWindow
from view.mainwindow import MainWindow
from model.editor import *;
##from model.ImportDOT import ImportDOT
from model.edge import *;
from model.node import *;
from model.selection import *;
from PyQt5.QtWidgets import *;
from PyQt5.QtWidgets import *;
from PyQt5.QtCore import *;
from PyQt5.QtGui import *;
import sys;


if __name__ == '__main__':

    all_nodes = {};
    all_edges = {};
    type_action = ""
    editeur = Editor(all_nodes, all_edges, type_action, 0, False);

    app = QApplication(sys.argv)
    mainWindow = MainWindow(editeur)
    sys.exit(app.exec_())
