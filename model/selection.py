##Copyright 2019, 2019 DUBETTIER Adrien adriendub@yahoo.fr
##Copyright 2019, 2019 LECLERC Benjamin benjamin.leclerc7@gmail.com
##
##This file is part of EdGraphZ.
##
##    EdGraphZ is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    EdGraphZ is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with EdGraphZ.  If not, see <https://www.gnu.org/licenses/>.

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import sys

##NOT USED IN THE ACTUAL PROJECT

class Selection:
    """
    A group of several elements (node or edge)

    """

    def __init__(self,list_node_selection):
        """
        The constructor

        :param list_node_selection: the first elements selected
        :type list_node_selection: List
        """
        self.list_node_selection = list_node_selection;

    def change_all_color(self,color):
        """
        Allow the user to change the color of all elements in list_node_selection

        :param color: the new color
        :type color: String

        """
        for x in self.list_node_selection:
            x.set_color(color);

    def move_all_element(self, new_position_x, new_position_y):
        """
        move all selected elements to a new position

        :param new_position_x: could be a float if scene position
        :type new_position_x: Int or Float
        :param new_position_y: could also be a float
        :type new_position_y: Int or Float

        """
        for element in self.list_node_selection:
            element.set_position_x(new_position_x);
            element.set_position_y(new_position_y);

    def delete_all(self, list_node):
        """
        delete all selected elements who are stored in list_node_selection
        """
        for element in self.list_node_selection:
            if element in list_node:
                list_node.remove_node_element(element);

    def add_node(self, node):
        """
        add a node to the list list_node_selection

        :param node: The new node to add
        :type node: Node

        """
        pass

