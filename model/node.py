##Copyright 2019, 2019 DUBETTIER Adrien adriendub@yahoo.fr
##Copyright 2019, 2019 LECLERC Benjamin benjamin.leclerc7@gmail.com
##
##This file is part of EdGraphZ.
##
##    EdGraphZ is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    EdGraphZ is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with EdGraphZ.  If not, see <https://www.gnu.org/licenses/>.

from model.element import *


class Node(Element):
    """
    An element that represent an Node
    """

    def __init__(self, position_x, position_y, label, color, form, id, old_id):
        """
        Constructor

        :param position_x: The actual position of the node element on the axe x
        :type position_x: Int or Float
        :param position_y: The actual position of the node element on the axe y
        :type position_y: int or Float
        :param label: The actual label of the node element
        :type label: String
        :param color: The actual color of the node element
        :type color: String(hexa)
        :param form: The actual form of the node element
        :type form: String
        :param id: The actual id of the node element
        :type id: Int
        :param old_id: The actual old id of the node element (can be None)
        :type old_id: String or Int
        """
        Element.__init__(self, label, color, form, id, old_id)
        self.position_x = position_x
        self.position_y = position_y