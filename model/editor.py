##Copyright 2019, 2019 DUBETTIER Adrien adriendub@yahoo.fr
##Copyright 2019, 2019 LECLERC Benjamin benjamin.leclerc7@gmail.com
##
##This file is part of EdGraphZ.
##
##    EdGraphZ is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    EdGraphZ is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with EdGraphZ.  If not, see <https://www.gnu.org/licenses/>.
from model.node import *
from model.edge import *
class Editor:
    """
    The main thing that represent all
    
    """

    def __init__(self, all_nodes, all_edges, type_action, num_id, is_oriented):
        """
        Constructor

        :param list_node: All the current node of the graph
        :type list_node: List
        :param list_edge: All the current edge of the graph
        :type list_edge: List
        :param type_action: The actual action on the graph
        :type type_action: String
        :param num_id: The actual if of the graph
        :type num_id: Int
        :param is_oriented: True or False
        :type is_oriented: Boolean
        """
        self.all_nodes = all_nodes
        self.all_edges = all_edges
        self.type_action = type_action
        self.num_id = num_id
        self.is_oriented = is_oriented

    def add_node_element(self, graphic_node, x, y, label, color, form, id, old_id):
        """
        add a node to the node list

        :param element: the node to add
        :type element Element
        """
        element = Node(x, y, label, color, form, id, old_id)
        self.all_nodes[element] = graphic_node

    def add_edge_element(self, graphic_edge, start_node, end_node, is_oriented, label, color, form, id, old_id):
        """
        add an edge to the edge list

        :param element: the edge to add
        :type element: Edge

        """
        element = Edge(start_node, end_node, is_oriented, label, color, form, id, old_id)
        self.all_edges[element] = graphic_edge

    def remove_node_element(self, element):
        """
        Used to remove a node from the node list

        :param element: the node to remove
        :type element: Node
        """
        del self.all_nodes[element]

    def remove_edge_element(self, element):
        """
        Used to remove an edge from the edge list

        :param element: the edge to remove
        :type element: Edge
        """
        del self.all_edges[element]

    def increment_num_id(self):
        """
        Increment the id and return it

        :return: Return the id
        :rtype: Int
        """
        self.num_id = self.num_id + 1
        return self.num_id
