##Copyright 2019, 2019 DUBETTIER Adrien adriendub@yahoo.fr
##Copyright 2019, 2019 LECLERC Benjamin benjamin.leclerc7@gmail.com
##
##This file is part of EdGraphZ.
##
##    EdGraphZ is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    EdGraphZ is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with EdGraphZ.  If not, see <https://www.gnu.org/licenses/>.

class Element:
    """
    An element can be of different type (edge, node)
    """

    def __init__(self, label, color, form, id, old_id):
        """
        Constructor

        :param label: Represent the name displayed on the graph
        :type label: String
        :param color: Represent the color of the element
        :type color: String
        :param form: Represent the form of the element (ex: dashed for edge or square for node)
        :type form: String
        :param id: Represent the actual id of the element
        :type id: Int
        :param old_id: Represent the old id if the element if it was imported
        :type old_id: String or Int
        """
        self.label = label
        self.color = color
        self.form = form
        self.id = id
        self.old_id = old_id