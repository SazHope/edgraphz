##Copyright 2019, 2019 DUBETTIER Adrien adriendub@yahoo.fr
##Copyright 2019, 2019 LECLERC Benjamin benjamin.leclerc7@gmail.com
##
##This file is part of EdGraphZ.
##
##    EdGraphZ is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    EdGraphZ is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with EdGraphZ.  If not, see <https://www.gnu.org/licenses/>.

from PyQt5.QtGui import QColor
from pydot import graph_from_dot_file

from view.mainwindow import GraphicsGraph
from model.edge import Edge
from model.node import Node


class ImportDOT(GraphicsGraph):

    def __init__(self, graph, editor, map_to_scene):
        """
        Constructor

        :param graph: The actual window (scene) of the Editor.
        :type graph: MainWindow
        :param current_state: The actual state of the model of the editor
        :type current_state: Editor
        :param pair_model_node: Connects the graphical interface to the model (GuiObjects: id)
        :type pair_model_node: Dictionary
        """
        super().__init__(editor, map_to_scene)
        self.graph = graph_from_dot_file(graph)
        self.editor = editor
        self.map_to_scene = map_to_scene

    def get_DOT(self, num):
        """
        We use pydot to parse dot file
        Read the dot file and add all element in the model
        :param num: Choose the graph you want to read in the dot file (If there are several)
        :type num: Int
        """
        generated_nodes = {}
        if self.graph[num].get_graph_type() == "graph":
            self.editor.is_oriented = False
        else:
            self.editor.is_oriented = True
        nodes = self.graph[num].get_node_list()
        pos_x = 20
        pos_y = 20
        for nod in nodes:
            if pos_x > 400:
                pos_x = 20
                pos_y += 80
            if isinstance(nod.get_name(), int) and int(nod.get_name()) > int(self.editor.id):
                self.editor.num_id = (int(nod.get_name()))
            self.editor.add_node_element(None, pos_x, pos_y, nod.get("label").replace('"', ''), QColor(nod.get("color").replace('"', '')), nod.get("shape").replace('"', ''), self.editor.increment_num_id(), nod.get_name())
            pos_x += 80
        edges = self.graph[num].get_edge_list()
        for edg in edges:
            if isinstance(edg.get_source(), int) and int(edg.get_source()) > self.editor.get_num_id():
                self.editor.num_id = (int(edg.get_source()))
            if isinstance(edg.get_destination(), int) and int(edg.get_destination()) > self.editor.id:
                self.editor.num_id = (int(edg.get_destination()))
            for i in self.editor.all_nodes.keys() :
                if i.old_id == edg.get_source() or i.id == edg.get_source():
                    temp = i
                if i.old_id == edg.get_destination() or i.id == edg.get_destination():
                    temp2 = i
            self.editor.add_edge_element(None, temp, temp2, self.editor.is_oriented, edg.get("label").replace('"', ''), QColor(edg.get("color").replace('"', '')), edg.get("style").replace('"',''),
                        self.editor.increment_num_id(), None)
        
        self.refresh_main_window()
